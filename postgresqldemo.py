import psycopg2
import pandas as pd
from sqlalchemy import create_engine
#engine = create_engine('postgresql+psycopg2://username:password@host:port/database')

try:
    connection = psycopg2.connect(
        user = "postgres",
        password="admin",
        host= "127.0.0.1",
        port = "5432",
        database = "postgres"
    )
    cursor = connection.cursor()
except (Exception, psycopg2.Error) as error :
    print ("Error while connecting to PostgreSQL", error)

create_table_query=("""
CREATE TABLE IF NOT EXISTS public."ChurninTelecoms"
(
    state text,
    account_length integer,
    area_code integer,
    phone_number text,
    international_plan text,
    voice_mail_plan text,
    number_vmail_messages integer,
    total_day_minutes float8,
    total_day_calls float8,
    total_day_charge float8,
    total_eve_minutes float8,
    total_eve_calls float8,
    total_eve_charge float8,
    total_night_minutes float8,
    total_night_calls float8,
    total_night_charge float8,
    total_intl_minutes float8,
    total_intl_calls float8,
    total_intl_charge float8,
    customer_service_calls integer,
    churn boolean);""")
cursor.execute(create_table_query)
connection.commit()

df = pd.read_csv("ChurnTelecoms.csv")
columns = df.columns
for index, row in df.iterrows():
    lis = []
    state = row["state"]
    account_length = row["account length"]
    area_code = row["area code"]
    phone_number = row["phone number"]
    international_plan = row["international plan"]
    voice_mail_plan = row["voice mail plan"]
    number_vmail_messages = row["number vmail messages"]
    total_day_minutes = row["total day minutes"]
    total_day_calls = row["total day calls"]
    total_day_charge = row["total day charge"]
    total_eve_minutes = row["total eve minutes"]
    total_eve_calls = row["total eve calls"]
    total_eve_charge = row["total eve charge"]
    total_night_minutes = row["total night minutes"]
    total_night_calls = row["total night calls"]
    total_night_charge = row["total night charge"]
    total_intl_minutes = row["total intl minutes"]
    total_intl_calls = row["total intl calls"]
    total_intl_charge = row["total intl charge"]
    customer_service_calls = row["customer service calls"]
    churn = row["churn"]
    lis.append(state)
    lis.append(account_length)
    lis.append(area_code)
    lis.append(phone_number)
    lis.append(international_plan)
    lis.append(voice_mail_plan)
    lis.append(number_vmail_messages)
    lis.append(total_day_minutes)
    lis.append(total_day_calls)
    lis.append(total_day_charge)
    lis.append(total_eve_minutes)
    lis.append(total_eve_calls)
    lis.append(total_eve_charge)
    lis.append(total_night_minutes)
    lis.append(total_night_calls)
    lis.append(total_night_charge)
    lis.append(total_intl_minutes)
    lis.append(total_intl_calls)
    lis.append(total_intl_charge)
    lis.append(customer_service_calls)
    lis.append(churn)
    print(lis)
    postgres_insert_query = """ 
    INSERT INTO "ChurninTelecoms" 
    (state,
    account_length,
    area_code,
    phone_number,
    international_plan,
    voice_mail_plan,
    number_vmail_messages,
    total_day_minutes,
    total_day_calls,
    total_day_charge,
    total_eve_minutes,
    total_eve_calls,
    total_eve_charge,
    total_night_minutes,
    total_night_calls,
    total_night_charge,
    total_intl_minutes,
    total_intl_calls,
    total_intl_charge,
    customer_service_calls,
    churn) 
    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    cursor.execute(postgres_insert_query, lis)
    connection.commit()

#search = cursor.execute("""SELECT * FROM "ChurninTelecoms" """)
#rows = search.fetchall()



