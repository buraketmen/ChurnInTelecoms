import pandas as pd
import numpy as np
from scipy import stats
from sklearn import preprocessing
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression, LogisticRegression, RidgeCV, LassoCV, Ridge, Lasso
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
from sklearn.metrics import auc, roc_auc_score, roc_curve, classification_report, confusion_matrix, matthews_corrcoef, accuracy_score
from sklearn.model_selection import train_test_split, learning_curve
from sklearn.neighbors import KNeighborsClassifier, kneighbors_graph
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import svm, datasets
from sklearn.feature_selection import SelectFromModel , RFE, chi2, SelectKBest
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
import statsmodels.formula.api as sm
from scipy.stats import ttest_ind
import matplotlib
from sklearn.neural_network import MLPClassifier
from sklearn.decomposition import PCA
import math
import scikitplot as skplt
from mlxtend.evaluate import lift_score


#Linear regression, Logistic regression, Decision Tree, KNN, Linear Discriminant Analysis, Gaussian Naive Bayes
df = pd.read_csv('ChurnTelecoms.csv', sep=',')
#print(df.head(10))
#print(df.isnull().sum())  #THERE IS NO NULL VALUE IN DATAFRAME
#print(df.describe()) #FOR DATA PROPERTIES
#print(df.shape)

def churncountone():
    plt.figure(1)
    y = df["churn"].value_counts()
    sns.barplot(y.index,y.values,ax=axes[0,0])

def churncounttwo():
    splot = sns.countplot(x="churn", data=df, ax=axes[0,0])  # FOR CHURN COUNT
    for p in splot.patches:
        splot.annotate(format(p.get_height(), '.0f'), (p.get_x() + p.get_width() / 2., p.get_height()), ha='center',
                       va='center', xytext=(0, 4), textcoords='offset points')

churn_True = df["churn"][df["churn"] == True]
print("Churn Pencentage= " +str((churn_True.shape[0]/ df["churn"].shape[0])*100) + "\n")

f, axes = plt.subplots(2,2,figsize=(10,8))
#churncountone()
churncountone()
df.groupby(["state", "churn"]).size().unstack().plot(kind='bar', stacked=True, figsize=(20,10))
df.groupby(["area code", "churn"]).size().unstack().plot(kind='bar', stacked=True, ax=axes[0,1])
df.groupby(["international plan", "churn"]).size().unstack().plot(kind='bar', stacked=True, ax=axes[1,0])
df.groupby(["voice mail plan", "churn"]).size().unstack().plot(kind='bar', stacked=True, ax=axes[1,1])
f.savefig('./Document/about churn.png')
#sns.pairplot(data=df, x_vars=['customer service calls','total minutes','total calls','total charge'], y_vars='churn', kind='reg')

df['international plan'] = df['international plan'].map({'yes': 1, 'no': 0}) #YES = 1, NO = 0
df['voice mail plan'] = df['voice mail plan'].map({'yes': 1, 'no': 0}) #YES = 1, NO = 0

"""label_encoder = LabelEncoder()
df.iloc[:,0] = label_encoder.fit_transform(df.iloc[:,0]).astype('float64')"""

state_list = df['state'].unique().tolist() ####################################################################################
state_mapping = dict( zip(state_list,range(len(state_list)))) ####################################################################################
df.replace({'state': state_mapping},inplace=True) ####################################################################################

#df.drop(['phone number','state'], axis=1, inplace=True)
#df.drop(['phone number'], axis=1, inplace=True)
df['total minutes'] = df['total day minutes'] + df['total eve minutes'] + df['total night minutes'] + df['total intl minutes']
df['total calls'] = df['total day calls'] + df['total eve calls'] + df['total night calls'] + df['total intl calls']
df['total charge'] = df['total day charge'] + df['total eve charge'] + df['total night charge'] + df['total intl charge']

#PREPARING THE DATA SET
new_df = df.filter(['churn','state','customer service calls','total minutes','total calls','total charge','account length','area code','international plan', 'voice mail plan','number vmail messages'],axis=1)
selected_columns = ['state','total calls','customer service calls','total minutes','total charge','account length','area code','international plan', 'voice mail plan','number vmail messages']

newdata = new_df.values
columns = new_df.columns
min_max_scaler = preprocessing.MinMaxScaler()
newdata_scaled = min_max_scaler.fit_transform(newdata)
df_for_feature = pd.DataFrame(data=newdata_scaled,columns=columns)
df_for_feature['churn'] = new_df['churn']

plt.figure(10, figsize=(10,8))
corr=df_for_feature.corr()
s = sns.heatmap(corr, annot=True)
plt.tight_layout()
#result: customer service calls, total minutes, total charge, international plan


def feature_selection_with_trees():
    X = df_for_feature[selected_columns]
    y = df_for_feature['churn']
    clf = ExtraTreesClassifier()
    clf = clf.fit(X, y)
    clflist = list(clf.feature_importances_)
    mapped = zip(selected_columns, clflist)
    mapped = set(mapped)
    print(mapped)
    # ('total calls', 0.07601549774109312), **************
    # ('total charge', 0.266762441092578), **************
    # ('total minutes', 0.16721646466708984), **************
    # ('area code', 0.03350194417313357),
    # ('international plan', 0.07187929610693082), **************
    # ('number vmail messages', 0.04290474586932995),
    # ('customer service calls', 0.16334975890455705), **************
    # ('voice mail plan', 0.02904647576070517),
    # ('state', 0.06823763693871657), **************
    # ('account length', 0.08108573874586586) **************

def feature_selection_with_linear():
    X = df_for_feature[selected_columns]
    y = df_for_feature['churn']
    scaler = preprocessing.MinMaxScaler()
    X = scaler.fit_transform(X)
    lm = LinearRegression()
    lm.fit(X, y)
    coef = pd.Series(lm.coef_, index=selected_columns)
    imp_coef = coef.sort_values()
    plt.figure(15)
    matplotlib.rcParams['figure.figsize'] = (8.0, 10.0)
    imp_coef.plot(kind="barh")
    plt.title("Feature importance using Linear Model")
    plt.tight_layout()
    print(list(zip(selected_columns, lm.coef_)))
    # ('state', 0.018116258488791424),
    # ('total calls', 0.02553740277149938),
    # ('customer service calls', 0.5248646139750702), **************
    # ('total minutes', -0.02028897585795902),
    # ('total charge', 0.5688441261181464), **************
    # ('account length', 0.023742221836877228),
    # ('area code', -0.00860030181046896),
    # ('international plan', 0.3024221606337113), **************
    # ('voice mail plan', -0.13495285141000743),
    # ('number vmail messages', 0.09579746733099005) **************

def feature_selection_with_logistic():
    X = df_for_feature[selected_columns]
    y = df_for_feature['churn']
    model = LogisticRegression()
    rfe = RFE(model, 1)
    fit = rfe.fit(X, y)
    print(list(zip(selected_columns, fit.ranking_)))
    # ('state', 7),
    # ('total calls', 9),
    # ('customer service calls', 2), **************
    # ('total minutes', 4), **************
    # ('total charge', 1), **************
    # ('account length', 10),
    # ('area code', 8),
    # ('international plan', 3), **************
    # ('voice mail plan', 5), **************
    # ('number vmail messages', 6)

def feature_selection_with_lasso():
    X = df_for_feature[selected_columns]
    y = df_for_feature['churn']
    reg = LassoCV()
    reg.fit(X, y)
    coef = pd.Series(reg.coef_, index=X.columns)
    imp_coef = coef.sort_values()
    plt.figure(11)
    matplotlib.rcParams['figure.figsize'] = (8.0, 10.0)
    imp_coef.plot(kind="barh")
    plt.title("Feature importance using Lasso Model")
    plt.tight_layout()
    #result: total charge, customer service calls, international plan

def feature_selection_with_ttest():
    for s in selected_columns:
        truedata= df_for_feature[df_for_feature['churn'] == True][s]
        falsedata = df_for_feature[df_for_feature['churn'] == False][s]
        _, pvalue = stats.ttest_ind(truedata, falsedata)
        print(str(s) +": %0.10f" %pvalue)
    #P-VALUES (<0.05)
    #state: 0.2057229492
    # total calls: 0.3616168365
    # customer service calls: 0.0000000000  **************
    # total minutes: 0.0000000000 **************
    # total charge: 0.0000000000 **************
    # account length: 0.3397600071
    # area code: 0.7215998968
    # international plan: 0.0000000000 **************
    # voice mail plan: 0.0000000034 **************
    # number vmail messages: 0.0000002118 **************

def feature_selection_with_univariate():
    X = df_for_feature[selected_columns]
    y = df_for_feature['churn']
    test = SelectKBest(score_func=chi2, k=4)
    fit = test.fit(X, y)
    np.set_printoptions(precision=3) # summarize scores
    print(list(zip(selected_columns, fit.scores_)))
    #('state', 0.28663182044321184),
    # ('total calls', 0.03847055799764465),
    # ('customer service calls', 17.86373200579029),
    # ('total minutes', 5.7563230488950525),
    # ('total charge', 7.368814148716919),
    # ('account length', 0.05969895121557807),
    # ('area code', 0.07661132579144936),
    # ('international plan', 203.24417753226783),
    # ('voice mail plan', 25.15695901074955),
    # ('number vmail messages', 12.169192129027017)

def feature_selection_with_PCA():
    X = df_for_feature[selected_columns]
    y = df_for_feature['churn']
    data_scaled = pd.DataFrame(data=preprocessing.scale(X), columns=selected_columns)
    pca = PCA(n_components=0.90, svd_solver='full')
    pca.fit_transform(data_scaled)
    print("Explained Variance: %s" % pca.explained_variance_ratio_.cumsum())
    df_pca = pd.DataFrame(pca.components_, columns=data_scaled.columns)
    print(df_pca)

#feature_selection_with_trees()
#feature_selection_with_linear()
#feature_selection_with_logistic()
#feature_selection_with_ttest()
#feature_selection_with_lasso()
#feature_selection_with_univariate()
#feature_selection_with_PCA()

#feature_names = ['customer service calls','total minutes','total charge','account length','area code','international plan', 'total calls']
feature_names = ['customer service calls','total charge','international plan']

X = df_for_feature[feature_names]
y = df_for_feature['churn']
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3, random_state=100)

def linearregmethod():
    linearreg= LinearRegression()
    linearreg.fit(X_train,y_train)
    print('Accuracy of Linear regression classifier on training set {:.5f}'.format(linearreg.score(X_train, y_train)))
    print('Accuracy of Linear regression classifier on test set {:.5f}\n'.format(linearreg.score(X_test, y_test)))

def logregmethod():
    logreg = LogisticRegression()
    logreg.fit(X_train, y_train)
    pred = logreg.predict(X_test)
    roccurve(logreg, "Logistic Regression", X_test, y_test)
    lr_conf_matrix = confusion_matrix(y_test, pred)
    plt.figure(15)
    plt.title('Logistic Regression Conf. Matrix')
    lrplot = sns.heatmap(lr_conf_matrix, annot=True, fmt='', cmap="Blues")
    plt.xlabel('Pred')
    plt.ylabel('Act')
    lrplot.figure.savefig('./Document/Logistic Regression Conf Matrix.png')
    print("LR Lift Score: " + str(lift_score(y_test,pred)))
    print("Logistic Regression MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of Logistic regression classifier on training set {:.5f}'.format(logreg.score(X_train, y_train)))
    print('Accuracy of Logistic regression classifier on test set {:.5f}\n'.format(logreg.score(X_test, y_test)))

def decisiontreemethod():
    global y_test
    dectree= DecisionTreeClassifier(max_depth=3)
    dectree.fit(X_train,y_train)
    pred = dectree.predict(X_test)
    predicted_probas = dectree.predict_proba(X_test)

    pred_df = pd.DataFrame(pred)
    predicted_df = pd.DataFrame(dectree.predict_proba(X_test))
    y_test_r = y_test.reset_index()
    predictions = pd.concat([y_test_r, pred_df,predicted_df], axis=1)
    predictions.columns = ['index', 'actual','predicted','falserate','truerate']
    predictions.sort_values(by=['truerate'], ascending=False,inplace=True)
    predictions.reset_index(inplace=True)
    predictions['decile'] = pd.qcut(predictions.index,10, labels=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'])
    predictions.drop(['level_0'], axis=1, inplace=True)
    newpredictions =predictions.loc[(predictions.decile == "1")]
    b = newpredictions.iloc[:,3:5].values
    rowcount = newpredictions.shape[0]
    true = 0
    for i in range(rowcount):
        if newpredictions.actual[i] == newpredictions.predicted[i]:
            true+=1
    percent = 100 * float(true)/float(rowcount)
    print(percent)
    skplt.metrics.plot_cumulative_gain(newpredictions.actual, b, title="Decision Tree Gain")
    skplt.metrics.plot_lift_curve(newpredictions.actual,b, title="Decision Tree Lift Curve")
    print("Decision Tree Lift Score (Ascending) %10 :" + str(lift_score(newpredictions.actual,newpredictions.predicted)))
    print("Decision Tree Lift Score (Ascending) %100  :" + str(lift_score(predictions.actual,predictions.predicted)))

    #skplt.metrics.plot_cumulative_gain(y_test, predicted_probas)
    #skplt.metrics.plot_lift_curve(y_test,predicted_probas, title="Decision Tree Lift Curve")
    plt.figure(3)
    dec_conf_matrix = confusion_matrix(y_test, pred)
    plt.title('Decision Tree Conf. Matrix')
    decisiontreeplot =sns.heatmap(dec_conf_matrix,annot=True,fmt='', cmap="BuPu")
    plt.xlabel('Pred')
    plt.ylabel('Act')
    decisiontreeplot.figure.savefig('./Document/Decision Tree Conf Matrix.png')
    roccurve(dectree, "Decision Tree", X_test, y_test)
    print("Decision Tree Lift Score %100 : " + str(lift_score(y_test,pred)))
    print("Decision Tree MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of Decision Tree classifier on training set {:.5f}'.format(dectree.score(X_train, y_train)))
    print('Accuracy of Decision Tree regression classifier on test set {:.5f}\n'.format(dectree.score(X_test, y_test)))
    #tree.export_graphviz(dectree)

def knnmethod():
    knn= KNeighborsClassifier(n_neighbors=12)
    knn.fit(X_train,y_train)
    pred = knn.predict(X_test)
    predicted_probas = knn.predict_proba(X_test)
    knn_conf_matrix = confusion_matrix(y_test, pred)
    skplt.metrics.plot_lift_curve(y_test,predicted_probas, title="KNN Lift Curve")
    plt.figure(4)
    plt.title('KNN Conf. Matrix')
    knnplot= sns.heatmap(knn_conf_matrix, annot=True, fmt='', cmap="Greens")
    knnplot.set_ylabel('Act')
    knnplot.set_xlabel('Pred')
    knnplot.figure.savefig('./Document/KNN Conf Matrix.png')
    roccurve(knn, "KNN", X_test, y_test)
    print("KNN Lift Score: " + str(lift_score(y_test,pred)))
    print("KNN MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of KNN classifier on training set {:.5f}'.format(knn.score(X_train, y_train)))
    print('Accuracy of KNN classifier on test set {:.5f}\n'.format(knn.score(X_test, y_test)))

def lineardiscmethod():
    lindis= LinearDiscriminantAnalysis()
    lindis.fit(X_train,y_train)
    pred = lindis.predict(X_test)
    roccurve(lindis, "Linear Discriminant Analysis", X_test, y_test)
    print("Linear Discriminant Analysis MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of Linear Discriminant Analysis classifier on training set {:.5f}'.format(lindis.score(X_train, y_train)))
    print('Accuracy of Linear Discriminant Analysis classifier on test set {:.5f}\n'.format(lindis.score(X_test, y_test)))

def gaussianmethod():
    gnb = GaussianNB()
    gnb.fit(X_train, y_train)
    pred = gnb.predict(X_test)
    roccurve(gnb, "Gaussian Naive Bayes", X_test, y_test)

    print("Gaussian Naive Bayes MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of Gaussian Naive Bayes classifier on training set {:.5f}'.format(gnb.score(X_train, y_train)))
    print('Accuracy of Gaussian Naive Bayes classifier on test set {:.5f}\n'.format(gnb.score(X_test, y_test)))

def supportvectormethod():
    svm = SVC()
    svm.fit(X_train,y_train)
    print('Accuracy of Support Vector Machine classifier on training set {:.5f}'.format(svm.score(X_train, y_train)))
    print('Accuracy of Support Vector Machine classifier on test set {:.5f}\n'.format(svm.score(X_test, y_test)))

def neuralnetwork():
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes = (10,10), random_state = 1)
    clf.fit(X_train, y_train)
    pred = clf.predict(X_test)
    roccurve(clf, "Neural Network", X_test, y_test)
    plt.figure(12)
    plt.title('Neural Network Conf. Matrix')
    nn_conf_matrix = confusion_matrix(y_test, pred)
    nnplot = sns.heatmap(nn_conf_matrix, annot=True, fmt='', cmap="YlGnBu")
    plt.xlabel('Pred')
    plt.ylabel('Act')
    nnplot.figure.savefig('./Document/Neural Network Conf Matrix.png')
    print("Neural Network MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of Neural Network classifier on training set {:.5f}'.format(clf.score(X_train, y_train)))
    print('Accuracy of Neural Network classifier on test set {:.5f}\n'.format(clf.score(X_test, y_test)))


def dectreefeatureimportance():
    dectree = DecisionTreeClassifier(max_depth=3)
    dectree.fit(X_train,y_train)
    feature_imp = dectree.feature_importances_
    feature_ser = pd.Series(feature_imp,index=feature_names)
    feature_ser = feature_ser.nlargest(10)
    plt.figure(5)
    plt.title("Decision Tree Feature Importance")
    decimportplot = feature_ser.plot(kind = 'barh', figsize=(10,10))
    decimportplot.figure.savefig('./Document/decisiontree feature importance.png')

def roccurve(classifier, legend, X_test, y_test):
    y_test_roc = np.array([([0, 1] if y else [1, 0]) for y in y_test])
    y_score = classifier.predict_proba(X_test)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(2):
        fpr[i], tpr[i], _ = roc_curve(y_test_roc[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test_roc.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    plt.figure(6)
    plt.plot(fpr["micro"], tpr["micro"], label=legend + "(AUC = %0.3f)" % roc_auc["micro"])
    plt.title("ROC Curve")
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.plot([0, 1], [0, 1], linestyle='--')
    plt.legend()

rocplot =plt.figure(6, figsize=(10,8))
linearregmethod()
logregmethod()
decisiontreemethod() #chosen
knnmethod() #chosen
lineardiscmethod()
gaussianmethod()
supportvectormethod()
neuralnetwork()

dectreefeatureimportance()
rocplot.savefig("./Document/ROC curve with method.png")
plt.tight_layout()
#plt.show()