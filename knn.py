import pandas as pd
import numpy as np
from scipy import stats
from sklearn import preprocessing
from sklearn.model_selection import KFold
from sklearn import metrics
from sklearn.metrics import auc, roc_auc_score, roc_curve, classification_report, confusion_matrix, matthews_corrcoef
from sklearn.model_selection import train_test_split, learning_curve
from sklearn.neighbors import KNeighborsClassifier, kneighbors_graph
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import svm, datasets

df = pd.read_csv('ChurnTelecoms.csv', sep=',')
#print(df.head(10))
#print(df.isnull().sum())  #THERE IS NO NULL VALUE IN DATAFRAME
#print(df.describe()) #FOR DATA PROPERTIES
#print(df.shape)

df['international plan'] = df['international plan'].map({'yes': 1, 'no': 0}) #YES = 1, NO = 0
df['voice mail plan'] = df['voice mail plan'].map({'yes': 1, 'no': 0}) #YES = 1, NO = 0

df.drop(['phone number'], axis=1, inplace=True)
df['total minutes'] = df['total day minutes'] + df['total eve minutes'] + df['total night minutes'] + df['total intl minutes']
df['total calls'] = df['total day calls'] + df['total eve calls'] + df['total night calls'] + df['total intl calls']
df['total charge'] = df['total day charge'] + df['total eve charge'] + df['total night charge'] + df['total intl charge']

#PREPARING THE DATA SET
new_df = df.filter(['churn','customer service calls','total minutes','total calls','total charge','account length','area code','international plan', 'voice mail plan','number vmail messages'],axis=1)
#feature_names = ['customer service calls','total minutes','total calls','total charge'] #TEST
feature_names = ['customer service calls','total charge','international plan']
X = new_df[feature_names]
y = new_df['churn'] #TARGET
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3, random_state=100)

#NORMALIZATION
scaler = preprocessing.MinMaxScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

def bestk():
    k_range = range(1, 26)
    scores = {}
    scores_list = []
    for k in k_range:
        knn = KNeighborsClassifier(n_neighbors=k)
        knn.fit(X_train, y_train)
        pred = knn.predict(X_test)
        scores[k] = metrics.accuracy_score(y_test, pred)
        scores_list.append(metrics.accuracy_score(y_test, pred))

    plt.figure(3)
    plt.plot(k_range, scores_list)
    plt.xlabel('Value of K for KNN')
    plt.ylabel('Testing Accuracy')

bestk()
knn= KNeighborsClassifier(n_neighbors=12)
knn.fit(X_train,y_train)
pred = knn.predict(X_test)

#knn_conf_matrix = confusion_matrix(y_test, pred)
#plt.figure(1)
#plt.title('KNN Conf. Matrix')
#sns.heatmap(knn_conf_matrix, annot=True, fmt='')

def roccurve(classifier, legend, X_test, y_test):
    y_test_roc = np.array([([0, 1] if y else [1, 0]) for y in y_test])
    y_score = classifier.predict_proba(X_test)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(2):
        fpr[i], tpr[i], _ = roc_curve(y_test_roc[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test_roc.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    plt.figure(2, figsize=(10,8))
    plt.plot(fpr["micro"], tpr["micro"], label=legend + "(AUC = %0.3f)" % roc_auc["micro"])
    plt.title("ROC Curve")
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.plot([0, 1], [0, 1], linestyle='--')
    plt.legend()

#roccurve(knn, "KNN", X_test, y_test)
plt.show()