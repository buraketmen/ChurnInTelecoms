import pandas as pd
import numpy as np
from scipy import stats
from sklearn import preprocessing
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
from sklearn.metrics import auc, roc_auc_score, roc_curve, classification_report, confusion_matrix, matthews_corrcoef
from sklearn.model_selection import train_test_split, learning_curve
from sklearn.neighbors import KNeighborsClassifier, kneighbors_graph
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import svm, datasets
import matplotlib.ticker as mtick

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=UserWarning)
sns.set(style="white")
"""
 COLUMNS            DATATYPE    UNIQUE DATA
customerID           object         7043  
gender               object         2
SeniorCitizen         int64         2
Partner              object         2
Dependents           object         2
tenure                int64         73
PhoneService         object         2
MultipleLines        object         3
InternetService      object         3
OnlineSecurity       object         3
OnlineBackup         object         3
DeviceProtection     object         3
TechSupport          object         3
StreamingTV          object         3
StreamingMovies      object         3
Contract             object         3
PaperlessBilling     object         2
PaymentMethod        object         4
MonthlyCharges      float64         1585
TotalCharges         object         6531
Churn                object         2
"""
columns = ['customerID', 'gender', 'SeniorCitizen', 'Partner', 'Dependents',
       'tenure', 'PhoneService', 'MultipleLines', 'InternetService',
       'OnlineSecurity', 'OnlineBackup', 'DeviceProtection', 'TechSupport',
       'StreamingTV', 'StreamingMovies', 'Contract', 'PaperlessBilling',
       'PaymentMethod', 'MonthlyCharges', 'TotalCharges', 'Churn']

numeric_features = ['tenure','MonthlyCharges','TotalCharges']

data = pd.read_csv("TelcoCustomerChurn.csv", sep=",")
for f in columns:
    data[f] = data[f].replace(" ",np.nan)                           #11 ROWS HAVE NAN VALUE (IN TOTALCHARGES)
data['TotalCharges'] = data['TotalCharges'].astype(float)                   #DATATYPE WAS CHANGED TO USE FILLNA METHOD
data['TotalCharges'].fillna((data['TotalCharges'].mean()))    #NAN DATA WAS FILLED BY USING MEAN METHOD
data.dropna(inplace=True)
replace_cols = [ 'OnlineSecurity', 'OnlineBackup', 'DeviceProtection',
                'TechSupport','StreamingTV', 'StreamingMovies']
for i in replace_cols :
    data[i]  = data[i].replace({'No internet service' : 'No'})

data["SeniorCitizen"] = data["SeniorCitizen"].replace({1: "Yes", 0: "No"})

def countplot(columns):
    for column in columns:
        splot = sns.countplot(x=column, data=data)  # FOR CHURN COUNT
        plt.show()

def kdeplot(columns):
    for column in columns:
        plt.figure(figsize=(8, 4))
        plt.title("Change of Churn for {}".format(column))
        sns.kdeplot(data[data['Churn'] == 'No'][column].dropna(), color= 'navy', label= 'Churn: No')
        sns.kdeplot(data[data['Churn'] == 'Yes'][column].dropna(), color= 'orange', label= 'Churn: Yes')
        plt.show()

def pieplot(columns):
    for column in columns:
        ax = (data[column].value_counts() * 100.0 / len(data)) \
            .plot.pie(autopct='%.2f%%', labels=['No', 'Yes'], figsize=(6, 6), fontsize=15)
        ax.set_title('Percentage of {}'.format(str(column)), fontsize=12)
        plt.show()

def examples():
    ax = sns.distplot(data['tenure'], hist=True, kde=False, bins=int(180 / 5), color='darkblue',
                      hist_kws={'edgecolor': 'black'}, kde_kws={'linewidth': 4})
    ax.set_ylabel('Count of Customers')
    ax.set_xlabel('Tenure (months)')
    ax.set_title('Count of Customers by their tenure')
    plt.show()

    ax = data['Contract'].value_counts().plot(kind='bar', rot=0, width=0.3)
    ax.set_ylabel('Count of Customers')
    ax.set_title('Count of Customers by Contract Type')
    plt.tight_layout()
    plt.show

    data.groupby(["PaymentMethod", "Churn"]).size().unstack().plot(kind='bar', stacked=True, figsize=(5, 5))
    plt.tight_layout()
    plt.show()

pie_features = ['Churn','SeniorCitizen','PhoneService']
kdeplot(numeric_features)
pieplot(pie_features)
countplot(pie_features)
examples()

data["gender"] = data["gender"].replace({"Female": 1, "Male": 0}).astype(float)
data["SeniorCitizen"] = data["SeniorCitizen"].replace({"Yes": 1, "No": 0}).astype(float)
data["Partner"] = data["Partner"].replace({"Yes": 1, "No": 0}).astype(float)
data["Dependents"] = data["Dependents"].replace({"Yes": 1, "No": 0}).astype(float)
data["PhoneService"] = data["PhoneService"].replace({"Yes": 1, "No": 0}).astype(float)
data["MultipleLines"] = data["MultipleLines"].replace({"Yes": 1, "No": 0, "No phone service": 2}).astype(float)
data["InternetService"] = data["InternetService"].replace({"No":0, "DSL": 1, "Fiber optic": 2}).astype(float)
data["OnlineSecurity"] = data["OnlineSecurity"].replace({"Yes": 1, "No": 0}).astype(float)
data["OnlineBackup"] = data["OnlineBackup"].replace({"Yes": 1, "No": 0}).astype(float)
data["DeviceProtection"] = data["DeviceProtection"].replace({"Yes": 1, "No": 0}).astype(float)
data["TechSupport"] = data["TechSupport"].replace({"Yes": 1, "No": 0}).astype(float)
data["StreamingTV"] = data["StreamingTV"].replace({"Yes": 1, "No": 0}).astype(float)
data["StreamingMovies"] = data["StreamingMovies"].replace({"Yes": 1, "No": 0}).astype(float)
data["Contract"] = data["Contract"].replace({"Month-to-month": 0, "One year": 1, "Two year": 2}).astype(float)
data["PaperlessBilling"] = data["PaperlessBilling"].replace({"Yes": 1, "No": 0}).astype(float)
data["PaymentMethod"] = data["PaymentMethod"].replace({"Mailed check": 0, "Credit card (automatic)": 1, "Bank transfer (automatic)": 2, "Electronic check":3 }).astype(float)
data["Churn"] = data["Churn"].replace({"Yes":True,"No":False}).astype("bool")

feature_names = ['gender', 'SeniorCitizen', 'Partner', 'Dependents',
            'tenure', 'PhoneService', 'MultipleLines', 'InternetService', 'OnlineSecurity',
            'OnlineBackup', 'DeviceProtection', 'TechSupport', 'StreamingTV', 'StreamingMovies',
            'Contract', 'PaperlessBilling', 'PaymentMethod', 'MonthlyCharges', 'TotalCharges']
X = data[feature_names]
y = data['Churn'] #TARGET
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3, random_state=100)

#NORMALIZATION
scaler = preprocessing.MinMaxScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

def linearregmethod():
    linearreg= LinearRegression()
    linearreg.fit(X_train,y_train)
    print('Accuracy of Linear regression classifier on training set {:.5f}'.format(linearreg.score(X_train, y_train)))
    print('Accuracy of Linear regression classifier on test set {:.5f}\n'.format(linearreg.score(X_test, y_test)))

def logregmethod():
    logreg = LogisticRegression()
    logreg.fit(X_train, y_train)
    pred = logreg.predict(X_test)
    fpr, tpr, thresholds = roc_curve(y_test, pred)
    roccurve(logreg, "Logistic Regression", X_test, y_test)
    print("Logistic Regression MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of Logistic regression classifier on training set {:.5f}'.format(logreg.score(X_train, y_train)))
    print('Accuracy of Logistic regression classifier on test set {:.5f}\n'.format(logreg.score(X_test, y_test)))

def decisiontreemethod():
    dectree= DecisionTreeClassifier(max_depth=8)
    dectree.fit(X_train,y_train)
    pred = dectree.predict(X_test)
    plt.figure(3)
    dec_conf_matrix = confusion_matrix(y_test, pred)
    plt.title('Decision Tree Conf. Matrix')
    decisiontreeplot =sns.heatmap(dec_conf_matrix,annot=True,fmt='')
    decisiontreeplot.figure.savefig('./Document/Decision Tree Conf Matrix.png')
    roccurve(dectree, "Decision Tree", X_test, y_test)

    print("Decision Tree MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of Decision Tree classifier on training set {:.5f}'.format(dectree.score(X_train, y_train)))
    print('Accuracy of Decision Tree regression classifier on test set {:.5f}\n'.format(dectree.score(X_test, y_test)))
    #tree.export_graphviz(dectree)

def knnmethod():
    knn= KNeighborsClassifier(n_neighbors=12)
    knn.fit(X_train,y_train)
    pred = knn.predict(X_test)

    knn_conf_matrix = confusion_matrix(y_test, pred)
    plt.figure(4)
    plt.title('KNN Conf. Matrix')
    knnplot= sns.heatmap(knn_conf_matrix, annot=True, fmt='')
    knnplot.figure.savefig('./Document/KNN Conf Matrix.png')

    roccurve(knn, "KNN", X_test, y_test)

    print("KNN MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of KNN classifier on training set {:.5f}'.format(knn.score(X_train, y_train)))
    print('Accuracy of KNN classifier on test set {:.5f}\n'.format(knn.score(X_test, y_test)))

def lineardiscmethod():
    lindis= LinearDiscriminantAnalysis()
    lindis.fit(X_train,y_train)
    pred = lindis.predict(X_test)
    roccurve(lindis, "Linear Discriminant Analysis", X_test, y_test)

    print("Linear Discriminant Analysis MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of Linear Discriminant Analysis classifier on training set {:.5f}'.format(lindis.score(X_train, y_train)))
    print('Accuracy of Linear Discriminant Analysis classifier on test set {:.5f}\n'.format(lindis.score(X_test, y_test)))

def gaussianmethod():
    gnb = GaussianNB()
    gnb.fit(X_train, y_train)
    pred = gnb.predict(X_test)
    roccurve(gnb, "Gaussian Naive Bayes", X_test, y_test)

    print("Gaussian Naive Bayes MCC: " + str(matthews_corrcoef(y_test, pred)))
    print('Accuracy of Gaussian Naive Bayes classifier on training set {:.5f}'.format(gnb.score(X_train, y_train)))
    print('Accuracy of Gaussian Naive Bayes classifier on test set {:.5f}\n'.format(gnb.score(X_test, y_test)))

def supportvectormethod():
    svm = SVC()
    svm.fit(X_train,y_train)
    print('Accuracy of Support Vector Machine classifier on training set {:.5f}'.format(svm.score(X_train, y_train)))
    print('Accuracy of Support Vector Machine classifier on test set {:.5f}\n'.format(svm.score(X_test, y_test)))

def dectreefeatureimportance():
    dectree = DecisionTreeClassifier(max_depth=3)
    dectree.fit(X_train,y_train)
    feature_imp = dectree.feature_importances_
    feature_ser = pd.Series(feature_imp,index=feature_names)
    feature_ser = feature_ser.nlargest(10)
    plt.figure(5)
    plt.title("Decision Tree Feature Importance")
    decimportplot = feature_ser.plot(kind = 'barh', figsize=(10,10))
    decimportplot.figure.savefig('./Document/decisiontree feature importance.png')

def roccurve(classifier, legend, X_test, y_test):
    y_test_roc = np.array([([0, 1] if y else [1, 0]) for y in y_test])
    y_score = classifier.predict_proba(X_test)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(2):
        fpr[i], tpr[i], _ = roc_curve(y_test_roc[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test_roc.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    plt.figure(6)
    plt.plot(fpr["micro"], tpr["micro"], label=legend + "(AUC = %0.3f)" % roc_auc["micro"])
    plt.title("ROC Curve")
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.plot([0, 1], [0, 1], linestyle='--')
    plt.legend()

rocplot =plt.figure(6, figsize=(10,8))

linearregmethod()
logregmethod()
decisiontreemethod() #chosen
knnmethod() #chosen
lineardiscmethod()
gaussianmethod()
supportvectormethod()
dectreefeatureimportance()
rocplot.savefig("./DocumentForNewData/ROC curve with method.png")
plt.show()