# ChurnInTelecoms
analyze of telecom data set https://www.kaggle.com/becksddf/churn-in-telecoms-dataset

YOU CAN CHECK THE SIMPLE DASHBOARD FROM
https://churnintelecoms-dashboard.herokuapp.com/

![alt text](https://github.com/buraketmen/ChurnInTelecoms/blob/master/Document/about%20churn.png)
![alt text](https://github.com/buraketmen/ChurnInTelecoms/blob/master/Document/corr.png)
![alt text](https://github.com/buraketmen/ChurnInTelecoms/blob/master/Document/ROC%20curve%20with%20method.png)
